#: ## Lab 7 ##
#:
#: CS-2911 Network Protocols
#: Dr. Yoder
#: Fall quarter 2016-2017
#:
#: | Team members (username) |
#: |:------------------------|
#: | Grace Hopper (hopperg)  |
#: | Donald Knuth (knuthd)   |
#:
#: Thanks to Trip Horbinski from the Fall 2015 class for providing the password-entering functionality.

# GUI library for password entry
import tkinter as tk

# Socket library
import socket

# SSL/TLS library
import ssl

# base-64 encode/decode
import base64

# Python date/time and timezone modules
import datetime
import time
import pytz
import tzlocal

# Module for reading password from console without echoing it
import getpass

# Modules for some file operations
import os
import mimetypes

# Host name for MSOE (hosted) SMTP server
SMTP_SERVER = 'smtp.office365.com'

# The default port for STARTTLS SMTP servers is 587
SMTP_PORT = 587

# SMTP domain name
SMTP_DOMAINNAME = 'msoe.edu'


def main():
    """Main test method to send an SMTP email message.

    Modify data as needed/desired to test your code,
    but keep the same interface for the smtp_send
    method.
    """
    (username, password) = login_gui()

    message_info = {}
    message_info['To'] = 'christieck@msoe.edu'
    message_info['From'] = username
    message_info['Subject'] = 'Yet another test message'
    message_info['Date'] = 'Thu, 9 Oct 2014 23:56:09 +0000'
    message_info['Date'] = get_formatted_date()

    print("message_info =", message_info)

    message_text = 'Test message_info number 6\r\n\r\nAnother line.'

    smtp_send(password, message_info, message_text)


def login_gui():
    """Creates a graphical user interface for secure user authorization.

    Returns:
        username_value -- The username as a string.
        password_value -- The password as a string.

    Author: Tripp Horbinski
    """
    gui = tk.Tk()
    gui.title("MSOE Email Client")
    center_gui_on_screen(gui, 370, 120)

    tk.Label(gui, text="Please enter your MSOE credentials below:") \
        .grid(row=0, columnspan=2)
    tk.Label(gui, text="Email Address: ").grid(row=1)
    tk.Label(gui, text="Password:         ").grid(row=2)

    username = tk.StringVar()
    username_input = tk.Entry(gui, textvariable=username)
    username_input.grid(row=1, column=1)

    password = tk.StringVar()
    password_input = tk.Entry(gui, textvariable=password, show='*')
    password_input.grid(row=2, column=1)

    auth_button = tk.Button(gui, text="Authenticate", width=25, command=gui.destroy)
    auth_button.grid(row=3, column=1)

    gui.mainloop()

    username_value = username.get()
    password_value = password.get()

    return username_value, password_value


def center_gui_on_screen(gui, gui_width, gui_height):
    """Centers the graphical user interface on the screen.

    Args:
        gui: The graphical user interface to be centered.
        gui_width: The width of the graphical user interface.
        gui_height: The height of the graphical user interface.

    Returns:
        The graphical user interface coordinates for the center of the screen.

    Author: Tripp Horbinski
    """
    screen_width = gui.winfo_screenwidth()
    screen_height = gui.winfo_screenheight()
    x_coord = (screen_width / 2) - (gui_width / 2)
    y_coord = (screen_height / 2) - (gui_height / 2)

    return gui.geometry('%dx%d+%d+%d' % (gui_width, gui_height, x_coord, y_coord))

# *** Do not modify code above this line ***


def smtp_send(password, message_info, message_text):
    """Send a message via SMTP.

    Args:
        password: String containing user password.
        message_info: Dictionary with string values for the following keys:
                'To': Recipient address (only one recipient required)
                'From': Sender address
                'Date': Date string for current date/time in SMTP format
                'Subject': Email subject
            Other keys can be added to support other email headers, etc.
    """

    server_info = (SMTP_SERVER, SMTP_PORT)
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.connect(server_info)
    print(read_message(connection))
    send_header(connection)
    connection.send(b'STARTTLS\r\n')
    print(read_message(connection))

    context = ssl.create_default_context()
    wrapped_socket = context.wrap_socket(connection,server_hostname=SMTP_SERVER)

    authenticate_user(wrapped_socket, password, message_info)
    send_message_headers(wrapped_socket, message_info)
    send_message(wrapped_socket, message_info, message_text)


# sends the header of the message
#   connection -- represents the connection between the server and client
# Written By: Jamie Doro and Connor Christie
def send_header(connection):
    connection.send(('EHLO %s\r\n'%SMTP_DOMAINNAME).encode())
    print(read_message(connection))


# authenticates the user to be able to send an email
#   connection -- represents the connection between the server and client
#   password -- the users password
#   message_info -- Dictionary with string values for 'to, 'from', 'date', 'subject
# Written By: Jamie Doro and Connor Christie
def authenticate_user(connection, password, message_info):
    send_header(connection)
    connection.send(b'AUTH LOGIN\r\n')
    print (read_line(connection))
    connection.send(base64.b64encode(message_info["From"].encode())+b"\r\n")
    print(read_line(connection))
    connection.send(base64.b64encode(password.encode())+b"\r\n")
    print(read_line(connection))


# sends the headers of the message
#   connection -- represents the connection between the server and client
#   message_info -- Dictionary with string values for 'to, 'from', 'date', 'subject
# Written By: Jamie Doro and Connor Christie
def send_message_headers(connection, message_info):
    connection.send(('MAIL FROM: <%s>\r\n' % message_info["From"]).encode())
    print(read_line(connection))
    connection.send(('RCPT TO: <%s>\r\n' % message_info["To"]).encode())
    print(read_line(connection))


# sends the content of the email
#   connection -- represents the connection between the server and client
#   message_info -- Dictionary with string values for 'to, 'from', 'date', 'subject
#   message_text -- The contents of the email
# Written By: Jamie Doro and Connor Christie
def send_message(connection, message_info, message_text):
    connection.send(b'DATA\r\n')
    print(read_line(connection))
    connection.send(("Subject: %s\r\n\r\n"%message_info["Subject"]).encode())
    connection.send(message_text.encode())
    connection.send(b'\r\n.\r\n')
    print(read_line(connection))
    connection.send(b"QUIT\r\n")
    print(read_line(connection))


# Reads bytes until a CR LF is read
# Returns the content of the whole line
#   connection -- represents the connection between the client and server
# Written By: Jamie Doro
def read_line(connection):
    message = ""
    while not message.endswith("\r\n"):
        message += next_character(connection)

    return message.replace("\r\n", "")


# Returns the next character for each byte
# Written By: Jamie Doro and Connor Christie
def next_character(connection):
    return chr(int.from_bytes(next_byte(connection), 'big'))


# reads the message from the server
#   connection -- represents the connection between the client and server
# Written By: Jamie Doro and Connor Christie
def read_message(connection):
    message = []
    current_line = read_line(connection)

    while "-" in current_line:
        message.append(parse_status_message(current_line, "-"))
        current_line = read_line(connection)

    message.append(parse_status_message(current_line, " "))

    return message


# parses the status massage into the two main parts of each line
#   current_line -- the current ine of the message
#   split_char -- the character that each line should split at
# Written By: Jamie Doro and Connor Christie
def parse_status_message(current_line, split_char):
    parts = current_line.split(split_char)
    return (parts[0], parts[1])


# Read the next byte from the socket data_socket.
# The data_socket argument should be an open tcp data connection socket, not a tcp listening socket.
# Returns the next byte, as a bytes object of one character.
# If the byte is not yet available, this method blocks (waits)
#   until the byte becomes available.
# If there are no more bytes, this method blocks indefinitely.
def next_byte(connection):
    return connection.recv(1)

# Your code and additional functions go here. (Replace this line, too.)

# ** Do not modify code below this line. **

# Utility functions
# You may use these functions to simplify your code.


def get_formatted_date():
    """Get the current date and time, in a format suitable for an email date header.

    The constant TIMEZONE_NAME should be one of the standard pytz timezone names.
    If you really want to see them all, call the print_all_timezones function.

    tzlocal suggested by http://stackoverflow.com/a/3168394/1048186

    See RFC 5322 for details about what the timezone should be
    https://tools.ietf.org/html/rfc5322

    Returns:
         Formatted current date/time value, as a string.
    """
    zone = tzlocal.get_localzone()
    print("zone =", zone)
    timestamp = datetime.datetime.now(zone)
    timestring = timestamp.strftime('%a, %d %b %Y %H:%M:%S %z')  # Sun, 06 Nov 1994 08:49:37 +0000
    return timestring


def print_all_timezones():
    """Print all pytz timezone strings.
    """
    for tz in pytz.all_timezones:
        print(tz)


# You probably won't need the following methods, unless you decide to
# try to handle email attachments or send multi-part messages.
# These advanced capabilities are not required for the lab assignment.

def get_mime_type(file_path):
    """Try to guess the MIME type of a file (resource), given its path (primarily its file extension)

    Args:
        file_path: String containing path to (resource) file, such as './abc.jpg'

    Returns:
        If successful in guessing the MIME type, a string representing the content
          type, such as 'image/jpeg'
        Otherwise, None
    """
    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


def get_file_size(file_path):
    """Try to get the size of a file (resource) in bytes, given its path

    Args:
        file_path -- String containing path to (resource) file, such as './abc.html'

    Returns:
        If file_path designates a normal file, an integer value representing the the file size in bytes
        Otherwise (no such file, or path is not a file), None
    """
    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()